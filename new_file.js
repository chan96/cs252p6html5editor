/**
 * @author chan96
 */
var previewBox = document.getElementById('previewBox');
var userBox = document.getElementById('userBox');

var htmlButton = document.getElementById('htmlButton');
var cssButton = document.getElementById('cssButton');
var jsButton = document.getElementById('jsButton');

var htmlButtonColor = '#00ff99';
var cssButtonColor = '#66ccff';
var jsButtonColor = '#ffcc66';

var textareas = document.getElementsByTagName('textarea');
var initialHtml = '<!DOCTYPE html>\n<html>\n\t<head>\n\t\t<meta charset="UTF-8">\n\t\t<title>Title of the document</title>\n\t</head>\n\t<body>\n\t\tDocument Content\n\t</body>\n</html>';
var currentHtml = initialHtml;
var currentJS = "";
var currentCSS = "";
var currentTab = 'html';

var connection;
var dialog = document.getElementById('loginWindow');
var userNameBox = document.getElementById('userNameBox');
var passwordBox = document.getElementById('passwordBox');
var loggedIn = 0;
var statusLabel = document.getElementById('statusLabel');

window.onload = function() {
	userBox.value = initialHtml;

	previewBox.src = "data:text/html;charset=utf-8," + escape(initialHtml);

	//enable tabs and update previewBox
	var count = textareas.length;
	for (var i = 0; i < count; i++) {
		textareas[i].onkeydown = function(e) {
			if (e.keyCode == 9 || e.which == 9) {
				e.preventDefault();
				var s = this.selectionStart;
				this.value = this.value.substring(0, this.selectionStart) + "\t" + this.value.substring(this.selectionEnd);
				this.selectionEnd = s + 1;
			} else if (e.keyCode == 13) {
				storeCurrentText();
				updatePreviewBox();

			}
		};
	}

	loadFiles();
	userBox.value = currentHtml;
	storeCurrentText();
	updatePreviewBox();
};

function storeCurrentText() {
	if (currentTab == 'html') {
		currentHtml = userBox.value;
	} else if (currentTab == 'js') {
		currentJS = userBox.value;
	} else if (currentTab == 'css') {
		currentCSS = userBox.value;
	}
}

function loadLoadedText() {
	if (currentTab == 'html') {
		userBox.value = currentHtml;
	} else if (currentTab == 'js') {
		userBox.value = currentJS;
	} else if (currentTab == 'css') {
		userBox.value = currentCSS;
	}
}

htmlButton.onclick = function() {
	var color = htmlButtonColor;
	userBox.style.borderColor = color;
	previewBox.style.borderColor = color;

	storeCurrentText();
	userBox.value = currentHtml;
	currentTab = 'html';
};

cssButton.onclick = function() {
	var color = cssButtonColor;
	userBox.style.borderColor = color;
	previewBox.style.borderColor = color;

	storeCurrentText();
	userBox.value = currentCSS;
	currentTab = 'css';
};

jsButton.onclick = function() {
	var color = jsButtonColor;
	userBox.style.borderColor = color;
	previewBox.style.borderColor = color;

	storeCurrentText();
	userBox.value = currentJS;
	currentTab = 'js';
};

function updatePreviewBox() {
	var contents = currentHtml + '<style>' + currentCSS + '<\/style>' + '<script>' + currentJS + '<\/script>';
	previewBox.src = "data:text/html;charset=utf-8," + escape(contents);
}

function showLogin() {
	// Create SocketIO instance, connect
	var submitButton = document.getElementById('submitButton');
	var createButton = document.getElementById('createButton');

	dialog.show();
	submitButton.onclick = login;
	createButton.onclick = createAccount;
	document.getElementById('exitButton').onclick = function() {
		dialog.close();
	};
}

function checkBoxText() {
	if (userNameBox.value === '' || passwordBox.value === '') {
		return 0;
	}
	return 1;
}

function createAccount() {

	if (checkBoxText() === 0) {
		alert('Username and password field cannot be blank.');
		return;
	}

	var userName = userNameBox.value;
	var password = passwordBox.value;
	var connection = new WebSocket('ws://testserver10.mybluemix.net:80');

	connection.onopen = function() {
		console.log('connected! Creating account ' + userName + ' with password ' + password);
		connection.send('create ' + userName + ' ' + password + '|');

	};

	// Log errors
	connection.onerror = function(error) {
		console.log('WebSocket Error ' + error);
		alert("Unable to contact server. Check internet connection.");
	};

	// Log messages from the server
	connection.onmessage = function(e) {
		console.log('Server: ' + e.data);

		connection.close();

		if (e.data === 'user exists') {
			alert('User already exists. Please try a different username.');
		} else if (e.data === 'success') {
			dialog.close();
			alert('Successfully created account!');
			loggedIn = 1;
		}
	};
	clearTextBoxes();
}

function loadData() {
	if (loggedIn === 0) {
		alert("Login to load your files");
		return;
	}

	var confirmLoad = confirm("All current text will be overwitten. Continue?");
	if (confirmLoad == false) {
		return;
	}

	var userName = sessionStorage.getItem('userName');
	var password = sessionStorage.getItem('password');
	var connection = new WebSocket('ws://testserver10.mybluemix.net:80');
	var count = 0;

	connection.onopen = function() {
		console.log('connected!');

		connection.send('load ' + userName + ' ' + password + '|' + currentHtml);
		count++;

	};

	// Log errors
	connection.onerror = function(error) {
		console.log('WebSocket Error ' + error);
		alert("Unable to contact server. Check internet connection.");
	};

	connection.onmessage = function(e) {
		console.log('Server: ' + e.data);
		if (e.data === 'invalid username or password') {
			connection.close();
			dialog.close();
			alert('Incorrect username or password');
		} else {
			if (count === 1) {
				console.log(e.data);
				currentHtml = e.data;
				connection.send('load ' + userName + ' ' + password + '|' + currentJS);
				count++;
			} else if (count === 2) {
				console.log(e.data);
				currentJS = e.data;
				connection.send('load ' + userName + ' ' + password + '|' + currentCSS);
				count++;
			} else if (count === 3) {
				console.log(e.data);
				currentCSS = e.data;
				count = 0;

				loadLoadedText();
				updatePreviewBox();
				connection.close();
			}
		}
	};
	clearTextBoxes();
}

function login() {

	if (checkBoxText() === 0) {
		alert('Username and password field cannot be blank.');
		return;
	}

	sessionStorage.setItem("userName", userNameBox.value);
	sessionStorage.setItem("password", passwordBox.value);

	var userName = sessionStorage.getItem('userName');
	var password = sessionStorage.getItem('password');
	var connection = new WebSocket('ws://testserver10.mybluemix.net:80');

	connection.onopen = function() {
		console.log('connected! Logging in...');

		connection.send('login ' + userName + ' ' + password + '|');
	};

	// Log errors
	connection.onerror = function(error) {
		console.log('WebSocket Error ' + error);
		alert("Unable to contact server. Check internet connection.");
	};

	connection.onmessage = function(e) {
		console.log('Server: ' + e.data);
		if (e.data === 'invalid username or password') {
			connection.close();
			alert('Incorrect username or password');
		} else {

			connection.close();
			dialog.close();
			loggedIn = 1;
			statusLabel.innerHTML = 'Signed In: ' + sessionStorage.getItem('userName');
		}
	};
	clearTextBoxes();
}

function saveData() {

	if (loggedIn === 0) {
		alert("Login to load your files");
		return;
	}

	var userName = sessionStorage.getItem('userName');
	var password = sessionStorage.getItem('password');
	var connection = new WebSocket('ws://testserver10.mybluemix.net:80');
	var count = 0;
	var texts = ["html", "js", "css"];
	var fileData = [currentHtml, currentJS, currentCSS];

	connection.onopen = function() {
		console.log('connected! Sending html file..');
		console.log('username: ' + userName + ' password: ' + password);

		connection.send('save ' + userName + ' ' + password + '|' + 'html' + '|' + currentHtml);
		count++;

	};

	// Log errors
	connection.onerror = function(error) {
		console.log('WebSocket Error ' + error);
		alert("Unable to contact server. Check internet connection.");
	};

	connection.onmessage = function(e) {
		console.log('Server: ' + e.data);
		if (e.data === 'invalid username or password') {
			connection.close();
			dialog.close();
			alert('Incorrect username or password');
		} else {
			if (count === 1) {
				console.log('Saved file. Sending js file...');
				connection.send('save ' + userName + ' ' + password + '|' + 'js' + '|' + currentJS);
				count++;
			} else if (count === 2) {
				console.log('Saved file. Sending css file...');
				connection.send('save ' + userName + ' ' + password + '|' + 'css' + '|' + currentCSS);
				count++;
			} else if (count === 3) {
				console.log('Saved all files.');
				count = 0;

				connection.close();
				alert('saved!');
			}
		}
	};
	clearTextBoxes();
}

function clearTextBoxes() {
	userNameBox.value = '';
	passwordBox.value = '';
}

function loadFiles() {
	var storedHtml = localStorage.getItem("html");
	var storedJS = localStorage.getItem("js");
	var storedCSS = localStorage.getItem("css");

	if (storedHtml !== null) {
		currentHtml = storedHtml;

	}
	if (storedJS !== null) {
		currentJS = storedJS;
	}
	if (storedCSS !== null) {
		currentCSS = storedCSS;
	}
}
